/*
	Script for handling Captain's map display operations.
 */

//  Global variables for gate's list and number of selected gate. 
var gatesList;
var selectedGateNr;

// Method for Captain's view initialization.
function captainInit()
{
	getSync();
	getGates();
	window.onbeforeunload = function(){
		sendSync("false");
	};
}

// This method send information about Captain's synchronization with server.
function sendSync(sync)
{
	var xhttp = new XMLHttpRequest();
	var params = "sync="+sync;
	xhttp.open("POST", server+"/captainSync", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(params);
}

// This method is for synchronization with server.
function getSync()
{
	sendSync("true");
	var xhr = new XMLHttpRequest();
	xhr.onerror = function(e){
		document.getElementById("syncIcon").style.color = "black";
	};
	xhr.onreadystatechange = function(){ 
        if (xhr.readyState == 4 && xhr.status == 200){
			var response = xhr.responseText;
			showInfoAboutSync(response);
		}
    }
	xhr.open('GET', server+"/sync", true);
	xhr.send(null);
}

// This method shows information about Captain's synchronization with server
// by turning lighting icon yellow. When not synchronized - icon turns black.
function showInfoAboutSync(sync)
{
	if(sync == "true")
		document.getElementById("syncIcon").style.color = "#ffcc00";
	else
		document.getElementById("syncIcon").style.color = "black";
}

// This method gets information about gates from server.
function getGates()
{
	var xhr = new XMLHttpRequest();
	xhr.onerror = function(e){
		document.getElementById("syncIcon").style.color = "black";
	};
	xhr.onreadystatechange = function() { 
        if (xhr.readyState == 4 && xhr.status == 200){
			var response = xhr.responseText;
			gatesList = getGatesFromResponse(response);
			fillGatesList(gatesList);
		}
    }
	xhr.open('GET', server+"/gates", true);
	xhr.send(null);
}

// This method fills options for selecting gates in Captain's view.
function fillGatesList(options)
{
	var select = document.getElementById("gatesList"); 
	
	for(var i = 0; i < options.length; i++) {
		var opt = options[i].id;
		var el = document.createElement("option");
		el.textContent = opt;
		el.value = options[i].id;
		select.appendChild(el);
	}
}

// This method gets information about gates from server.
function getGatesFromResponse(response)
{
	var gatesList = new Array();
	var gatesLines = response.split("</br>");			// seperating lines
	for(var i = 0; i < gatesLines.length-1; i++) {		// last line is empty, so: length-1
		var gatesParams = gatesLines[i].split(" ");		// seperating 'id', 'lat' and 'lon'
		var gate = new Gate(gatesParams[0], gatesParams[1], gatesParams[2]);
		gatesList[i] = gate;
	}
	return gatesList;
}

// This method allows display of selected gate and taxiway.
function selectGate(){
	if(selectedGateNr != null){
		deleteGateTaxiWay();
	}
	selectedGateNr = document.getElementById("gatesList").selectedIndex-1;	// element indexed '0' is empty, so: selectedIndex-1
	var gate = gatesList[selectedGateNr];
	showGateAndTaxiWay(direction, gate);
}

// This method  send selected gate to server.
function sendGate()
{
    var xhttp = new XMLHttpRequest();
    var id = document.getElementById("gatesList").value; 
	if (selectedGateNr == null){
		alert("Select gate");
	}
	else{
		var id = gatesList[selectedGateNr].id;
		var params = "id="+id;
		xhttp.open("POST", server+"/selectedGate", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(params);
		alert("Sent gate");
	}
}

// This method is for deleting gate and taxiway. After this information about deleted items is sent to server.
function deleteGateAndTaxiWayAndSendInfo(){
	deleteGateTaxiWay();
	sendInfoAboutDeleteGateAndTaxiWay();
}

// This method sends information about deleted gate and taxiway to server for knowledge FirstOfficer.
function sendInfoAboutDeleteGateAndTaxiWay(){
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", server+"/removedGate", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("removed=true");
}