/*
 * Class Gate.
 * Keeps information about gate's 'id', 'lon', 'lat'.
 */
 
 class Gate {
	constructor(id, lon, lat) {
		this.id = id;
		this.lon = lon;
		this.lat = lat;
	}
}