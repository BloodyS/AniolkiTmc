/*
	Script for displaying First Officer's map view.
 */
 
var map;
var mercator = new OpenLayers.Projection("EPSG:900913");
var geographic = new OpenLayers.Projection("EPSG:4326");
var directionsService;
var markers;
var showTaxi = true;
var showGate = true;
var circleLayer;
var vector;
var myGate;

// This method deleted gate and taxiway from displayed map.
function deleteGateTaxiWay()
{	
	if(showTaxi != false && showGate != false){
	map.removeLayer(circleLayer);
	map.removeLayer(vector);}
	showTaxi = false;
	showGate = false;
}

var lineStyle = {
			strokeColor: "#EE0000",
			strokeOpacity: 0.7,
			strokeWidth: 3,
		};

var circleStyle = {
			strokeColor: "#FF0000",
			strokeOpacity: 0.8,
			strokeWidth: 2,
			fillColor: "#FF0000",
			fillOpacity: 0.4
		};

// Method for map initialization.			
function mapInit()
{
	var options = {
			projection: mercator,
			displayProjection: geographic,
			units: "m",
			maxResolution: 156543.0339,
			maxExtent: new OpenLayers.Bounds(-20037508.34, -20037508.34, 20037508.34, 20037508.34)
		};
		
	map = new OpenLayers.Map('map', options);
	var osm = new OpenLayers.Layer.OSM(); 
	var gmap = new OpenLayers.Layer.Google("Google", {sphericalMercator:true});
	markers = new OpenLayers.Layer.Markers("points");
	map.addLayers([osm, gmap]);
	map.addLayer(markers);

	directionsService = new google.maps.DirectionsService();

	map.addControl(new OpenLayers.Control.LayerSwitcher());
	map.addControl(new OpenLayers.Control.MousePosition());

	map.setCenter(new OpenLayers.LonLat(18.47305, 54.37645).transform(geographic, mercator), 15);
}

// This method is for displaying selected gate and taxiway.
function showGateAndTaxiWay(fromDirection, myGate)
{
	showSelectedGate(myGate);
	showTaxiWay(fromDirection, myGate.id);
}

// This method displays selected gate.
function showSelectedGate(myGate)
{
	var circle = OpenLayers.Geometry.Polygon.createRegularPolygon(
		new OpenLayers.Geometry.Point(myGate.lon, myGate.lat).transform(geographic, mercator),
		35, 40,	0);
	circleLayer = new OpenLayers.Layer.Vector();
	var featurecircle = new OpenLayers.Feature.Vector(circle, null, circleStyle);
	circleLayer.addFeatures([featurecircle]);
	map.addLayer(circleLayer);
}

// This method displays taxiway for selected gate.
function showTaxiWay(fromDirection, myGate)
{
	showTaxi = true;
	showGate = true;
	
	if (fromDirection == "East"){
	
		var p1 = new OpenLayers.Geometry.Point(18.49173, 54.37129).transform(geographic, mercator);
		var p2 = new OpenLayers.Geometry.Point(18.47046, 54.37654).transform(geographic, mercator);
		var p3 = new OpenLayers.Geometry.Point(18.46979, 54.37680).transform(geographic, mercator);
		var p4 = new OpenLayers.Geometry.Point(18.46903, 54.37715).transform(geographic, mercator);
		var p5 = new OpenLayers.Geometry.Point(18.46846, 54.37747).transform(geographic, mercator);
		var p6 = new OpenLayers.Geometry.Point(18.46598, 54.37934).transform(geographic, mercator);
		var p7 = new OpenLayers.Geometry.Point(18.46583, 54.37953).transform(geographic, mercator);
		var p8 = new OpenLayers.Geometry.Point(18.46578, 54.37969).transform(geographic, mercator);
		var p9 = new OpenLayers.Geometry.Point(18.46586, 54.37990).transform(geographic, mercator);
		var p10 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);
		var p11 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);
		var p12 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);
		var p13 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);
		var p14 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);
		var p15 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);
	}
	else{
			
		var p1 = new OpenLayers.Geometry.Point(18.45214, 54.38107).transform(geographic, mercator);
		var p2 = new OpenLayers.Geometry.Point(18.47158, 54.37626).transform(geographic, mercator);
		var p3 = new OpenLayers.Geometry.Point(18.47268, 54.37609).transform(geographic, mercator);
		var p4 = new OpenLayers.Geometry.Point(18.47340, 54.37601).transform(geographic, mercator);
		var p5 = new OpenLayers.Geometry.Point(18.47406, 54.37599).transform(geographic, mercator);
		var p6 = new OpenLayers.Geometry.Point(18.47482, 54.37598).transform(geographic, mercator);
		var p7 = new OpenLayers.Geometry.Point(18.47672, 54.37609).transform(geographic, mercator);
		var p8 = new OpenLayers.Geometry.Point(18.47696, 54.37615).transform(geographic, mercator);
		var p9 = new OpenLayers.Geometry.Point(18.47711, 54.37622).transform(geographic, mercator);
		var p10 = new OpenLayers.Geometry.Point(18.47721, 54.37654).transform(geographic, mercator);
		var p11 = new OpenLayers.Geometry.Point(18.47695, 54.37672).transform(geographic, mercator);
		var p12 = new OpenLayers.Geometry.Point(18.46617, 54.37939).transform(geographic, mercator);
		var p13 = new OpenLayers.Geometry.Point(18.46585, 54.37964).transform(geographic, mercator);
		var p14 = new OpenLayers.Geometry.Point(18.46582, 54.37979).transform(geographic, mercator);
		var p15 = new OpenLayers.Geometry.Point(18.46624, 54.38044).transform(geographic, mercator);		
	}	
		
	if (myGate == 20){
				
		var p16 = new OpenLayers.Geometry.Point(18.46644, 54.38061).transform(geographic, mercator);
		var p17 = new OpenLayers.Geometry.Point(18.46681, 54.38108).transform(geographic, mercator);
	}
	else{
			
		var p16 = new OpenLayers.Geometry.Point(18.46623, 54.38050).transform(geographic, mercator);
		var p17 = new OpenLayers.Geometry.Point(18.46618, 54.38059).transform(geographic, mercator);
		var p18 = new OpenLayers.Geometry.Point(18.46612, 54.38066).transform(geographic, mercator);
		var p19 = new OpenLayers.Geometry.Point(18.46606, 54.38070).transform(geographic, mercator);
			
		if (myGate == 21){
		
			var p20 = new OpenLayers.Geometry.Point(18.46588, 54.38074).transform(geographic, mercator);
			var p21 = new OpenLayers.Geometry.Point(18.46624, 54.38121).transform(geographic, mercator);		
		} 
		else if (myGate == 22){
		
			var p20 = new OpenLayers.Geometry.Point(18.46530, 54.38089).transform(geographic, mercator);
			var p21 = new OpenLayers.Geometry.Point(18.46566, 54.38136).transform(geographic, mercator);
		} 
		else if (myGate == 23){
			
			var p20 = new OpenLayers.Geometry.Point(18.46471, 54.38103).transform(geographic, mercator);
			var p21 = new OpenLayers.Geometry.Point(18.46507, 54.38151).transform(geographic, mercator);
		}
	}
	var taxi1 = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21];
	vector = new OpenLayers.Layer.Vector();
	vector.addFeatures([new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString(taxi1), null, lineStyle)]);
	map.addLayers([vector]);
}