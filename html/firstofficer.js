/*
	Script for handling First Officer's map display operations.
 */
 
//  Global variable for selected gate's number. 
var selectedGate;

// Method for First Officer's view initialization.
function firstOfficerInit()
{
	selectedGate = new Gate(0, 0.0, 0.0);
	getSync();
	checkCaptain();
	window.onbeforeunload = function(){
		sendSync("false");
	};
}

// This method send information about First Officer's synchronization with server.
function sendSync(sync)
{
	var xhttp = new XMLHttpRequest();
	var params = "sync="+sync;
	xhttp.open("POST", server+"/firstOfficerSync", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(params);
}

// This method is for synchronization with server.
function getSync()
{
	sendSync("true");
	var xhr = new XMLHttpRequest();
	xhr.onerror = function(e){
		document.getElementById("syncIcon").style.color = "black";
	};
	xhr.onreadystatechange = function() { 
		if (xhr.readyState == 4 && xhr.status == 200){
			var response = xhr.responseText;
			showInfoAboutSync(response);
		}
	}
	xhr.open('GET', server+"/sync", true);
	xhr.send(null);
}

// This method shows information about First Officer's synchronization with server
// by turning lighting icon yellow. When not synchronized - icon turns black.
function showInfoAboutSync(sync)
{
	if(sync == "true")
		document.getElementById("syncIcon").style.color = "#ffcc00";
	else
		document.getElementById("syncIcon").style.color = "black";
}

// This method gets information about Captain's choices (selected/deleted gate and taxiway).
function checkCaptain()
{
	getSelectedGate();
	getInfoAboutDeleteGateAndTaxiWay();
}

// This method gets information about selected by Captain gate.
function getSelectedGate()
{
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() { 
        if (xhr.readyState == 4 && xhr.status == 200){
			var response = xhr.responseText;
			var receivedGate = getGateFromResponse(response);
			if(isNewGate(receivedGate))
				showGateWithTaxiWayAndAlert(receivedGate);
		}
    }
	xhr.open('GET', server+"/selectedGate", true);
	xhr.send(null);
}

// This method gets gate from server.
function getGateFromResponse(response)
{
	var gateParams = response.split(" "); // rozdzielenie id, lat i lon
	var gate = new Gate(gateParams[0], gateParams[1], gateParams[2]);
	return gate;
}

// This method compares received gate with the one that's already known.
function isNewGate(receivedGate)
{
	if(receivedGate.id != selectedGate.id){
		return true;
	}
	else{
		return false;
	}
}

// This method shows alert about new gate chosen by Captain.
function showGateWithTaxiWayAndAlert(receivedGate)
{
	if(selectedGate.id != 0){
		deleteGateTaxiWay();		
	}
	selectedGate = receivedGate;
	alert("Received new chosen gate");
	showGateAndTaxiWay(direction, selectedGate);
}

// This method gets information about deleted by Captain gate and taxiway from server.
function getInfoAboutDeleteGateAndTaxiWay()
{
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() { 
        if (xhr.readyState == 4 && xhr.status == 200){
			var response = xhr.responseText;
			if(response == "true"){
				alert("Captain removed gate and taxiway");
				sendFalseToRemovedGate();
			}
		}
    }
	xhr.open('GET', server+"/removedGate", true);
	xhr.send(null);
}

// This method updates removed gate.
function sendFalseToRemovedGate()
{
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", server+"/removedGate", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("removed=false");
}