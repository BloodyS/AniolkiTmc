//testy zeby dzialaly trzeba dodac testy w mavenie i zmienic metode z private na public

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author student
 */
public class GateManagerTest extends TestCase {
    
    public GateManagerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSomeMethod() {

        GateManager gm = new GateManager();
        Gate testowy = new Gate(33,22,11);
        Assert.assertEquals("Sprawdzanie podziału spacjami:", gm.createGateFromString("33 22 11"), testowy);
        Assert.assertEquals("Sprawdzanie podziału podkresleniami:", gm.createGateFromString("33_22_11"), testowy);
    }

}
