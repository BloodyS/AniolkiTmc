/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/* 
 * Class Gate.
 * This class keeps information about gate's  id and location.
 */
public class Gate {
    public int id;
    public Location location; 
    
    public Gate(int id, double lat, double lon){
        this.id = id;
        location = new Location(lat, lon);
    }
}
