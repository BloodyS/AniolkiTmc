package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/*
 * Class GateManager.
 * Keeps information about gates.
 */

public class GateManager {
    public Map<Integer, Gate> gates;
    private final String filePath = "gates.txt";
    
    public void initialGateState(){
        gates = getGatesFromFile();
    }
    
    private Map<Integer, Gate> getGatesFromFile(){
        Map<Integer, Gate> newGates = new HashMap<>();
        File file = new File(filePath);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                Gate gate = createGateFromString(line);
                newGates.put(gate.id, gate);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return newGates;
    }
    
    // This method creates new instance of Gate class - contains information about gate's id and location.
    private Gate createGateFromString(String line){
        String[] params = line.split(" ");
        int id = Integer.parseInt(params[0]);
        double lat = Double.parseDouble(params[1]);
        double lon = Double.parseDouble(params[2]);
        return new Gate(id, lat, lon);
    }
}
