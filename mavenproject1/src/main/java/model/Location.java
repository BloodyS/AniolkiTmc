/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/*
 * Class Location.
 * This class keeps information about location's latitude and longtitude.
 */
public class Location {
   public double lat;
   public double lon;
   
   public Location(double lat, double lon){
       this.lat = lat;
       this.lon = lon;
   }
}
