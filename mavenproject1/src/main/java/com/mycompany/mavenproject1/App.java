package com.mycompany.mavenproject1;
import java.util.Map;
import model.Gate;
import model.GateManager;
import static spark.Spark.*;

public class App 
{
    public static boolean isCaptainSync = false;
    public static boolean isFirstOfficerSync = false;
    public static boolean areAllSync = false;
    public static Gate selectedGate = new Gate(0, 0, 0);
    public static boolean didCaptainRemoveGate = false;
    
    // Main mathod.
    public static void main(String[] args) {
        port(80);
        enableCORS();
        
        GateManager gateManager = new GateManager();
        gateManager.initialGateState();
        String gatesString = getGatesString(gateManager.gates);
        
        get("/sync", (req,res) -> areAllSync);
        post("/captainSync", (req, res) -> {
            String sync = req.queryParams("sync");
            updateSync(sync, "Captain");
            return sync;
        });
        post("/firstOfficerSync", (req, res) -> {
            String sync = req.queryParams("sync");
            updateSync(sync, "First officer");
            return sync;
        });
        
        get("/gates", (req, res) -> gatesString);
        
        get("/selectedGate", (req,res) -> getGateString(selectedGate));
        post("/selectedGate", (req, res) -> {
            String id;
            id = req.queryParams("id");
            updateSelectedGate(gateManager, id);
            return id;
        });
        
        get("/removedGate", (req,res) -> didCaptainRemoveGate);
        post("/removedGate", (req, res) -> {
            String removed = req.queryParams("removed");
            updateRemovedGate(gateManager, removed);
            return removed;
        });
    }
  
    // Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS() {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
        });
    }
    
    // This method returns string containing data about all gates.
    // Gates are seperated by new line (<br>)
    private static String getGatesString(Map<Integer, Gate> gates){
        StringBuilder gatesString = new StringBuilder();
        for(Map.Entry<Integer, Gate> gateWithId : gates.entrySet()){
            gatesString.append(getGateString(gateWithId.getValue()) + "</br>");
        }
        return gatesString.toString();
    }
    
    // This method gets information about single gate.
    private static String getGateString(Gate gate){
        return gate.id + " " + gate.location.lat + " " + gate.location.lon;
    }
    
    // This method updates information about synchronization of Captain and First Officer.
    private static void updateSync(String isSyncInfo, String client){
        boolean isSync = Boolean.valueOf(isSyncInfo);
        if(client.equals("Captain"))
            isCaptainSync = isSync;
        else if(client.equals("First officer"))
            isFirstOfficerSync = isSync;
        if(isCaptainSync && isFirstOfficerSync)
            areAllSync = true;
        else
            areAllSync = false;
    }
    
    // This method updates information about chosen gate.
    private static void updateSelectedGate(GateManager gateManager, String newSelectedGateId){
        Integer id = Integer.parseInt(newSelectedGateId);
        selectedGate = gateManager.gates.get(id);
        System.out.println("" + selectedGate.id);
    }
    
    // This method updates information about removed gate.
    private static void updateRemovedGate(GateManager gateManager, String removedGateInfo){
        didCaptainRemoveGate = Boolean.valueOf(removedGateInfo);
        System.out.println("" +  didCaptainRemoveGate);
    }
}